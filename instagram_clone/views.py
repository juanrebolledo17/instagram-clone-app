from django.shortcuts import redirect

def redirect_to_login(request):
	# return redirect('%s?next=%s' % ('account:login', request.path))
	if request.user.is_authenticated:
		return redirect('account:dashboard')
		
	return redirect('account:login')