from django.shortcuts import render, redirect, get_object_or_404
from .forms import RegistrationForm, UserEditForm, ProfileEditForm, PasswordChangeForm
from .models import CustomUser, Profile, Contact
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from django.contrib.auth.models import User
from django.contrib import messages
from publications.models import Publication, Comment
from publications.forms import CommentForm
from common.decorators import ajax_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponse, JsonResponse
from django.views.decorators.http import require_POST
import json
from actions.utils import create_action
from actions.models import Action

def register(request):
	if request.method == 'POST':
		registration_form = RegistrationForm(request.POST)
		if registration_form.is_valid():
			new_user = registration_form.save(commit=False)
			new_user.set_password(registration_form.cleaned_data['password'])
			new_user.save()
			profile = Profile.objects.create(user=new_user)
			create_action(new_user, 'has created an account')

			return redirect('account:dashboard')
	else:
		registration_form = RegistrationForm()

	return render(request, 'account/register.html', {'registration_form': registration_form})

@login_required
def edit(request):
	if request.method == 'POST':
		user_form = UserEditForm(instance=request.user, data=request.POST)
		profile_form = ProfileEditForm(instance=request.user.profile, data=request.POST, files=request.FILES)

		if user_form.is_valid() and profile_form.is_valid():
			user_form.save()
			profile_form.save()
			# messages.success(request, 'Profile updated!', extra_tags='is-primary')
			return redirect('account:edit')
		else:
			# messages.error(request, 'Invalid information!', extra_tags='is-danger')
			return redirect('account:edit')

	else:
		user_form = UserEditForm(instance=request.user)
		profile_form = ProfileEditForm(instance=request.user.profile)
		password_form = PasswordChangeForm(user=request.user)
	
	return render(request, 'account/edit_account.html', {'user_form': user_form, 'profile_form': profile_form, 'password_form': password_form})

@login_required
def change_password(request):
	if request.method == 'POST':
		password_form = PasswordChangeForm(user=request.user, data=request.POST)

		if password_form.is_valid():
			password_form.save()
			return redirect('account:edit')
	else:
		password_form = PasswordChangeForm(instance=request.user)
	
	user_form = UserEditForm(instance=request.user)
	profile_form = ProfileEditForm(instance=request.user.profile)

	return render(request, 'account/edit_account.html', {'user_form': user_form, 'profile_form': profile_form, 'password_form': password_form})

@login_required
def dashboard(request):
	# display all actions by default
	# actions = Action.objects.exclude(user=request.user)
	# following_ids = request.user.following.values_list('id', flat=True)
	# if following_ids:
	# 	# if user is following others, retrieve only their actions
	# 	actions = actions.filter(user_id__in=following_ids).select_related('user', 'user__profile').prefetch_related('target')
	# actions[:10]
	publications = Publication.objects.all()
	return render(request, 'account/dashboard.html', {'section': 'dashboard', 'publications': publications})


@login_required
def profile(request):
	return render(request, 'account/profile.html')

@login_required
def logout_view(request):
	logout(request)
	return redirect('account:login')

@login_required
def user_list(request):
	users = CustomUser.objects.filter(is_active=True)
	return render(request, 'account/user/list.html', {'section': 'explore', 'users': users})

@login_required
def user_detail(request, username):
	usr = get_object_or_404(CustomUser, username=username, is_active=True)
	return render(request, 'account/user/detail.html', {'section': 'explore', 'usr': usr})

@ajax_required
@require_POST
@login_required
def user_follow(request):
	data = json.loads(request.body.decode('utf-8'))
	user_id = data.get('id')
	action = data.get('action')
	if user_id and action:
		try:
			user = CustomUser.objects.get(id=user_id)
			if action == 'follow':
				Contact.objects.get_or_create(user_from=request.user, user_to=user)
				create_action(request.user, 'is following', user)
			else:
				Contact.objects.filter(user_from=request.user, user_to=user).delete()
			return JsonResponse({'status': 'ok'})
		except User.DoesNotExist:
			return JsonResponse({'status': 'ko'})
	return JsonResponse({'status': 'ko'})

@login_required
def redirect_to_profile(request):
	return redirect('account:profile')

# @login_required
# def publication_list(request):
# 	publications = Publication.objects.all()
# 	paginator = Paginator(publications, 6)
# 	page = request.GET.get('page')
# 	print(page)
# 	try:
# 		publications = paginator.page(page)
# 	except PageNotAnInteger:
# 		publications = paginator.page(1)
# 	except EmptyPage:
# 		if request.is_ajax():
# 			return HttpResponse('')
# 		publications = paginator.page(paginator.num_pages)
# 	if request.is_ajax():
# 		return render(request, 'includes/list_ajax.html', { 'publications': publications})
# 	return render(request, 'account/profile.html', {'publications': publications})

# @login_required
# def profile(request):
# 	publications = Publication.objects.all()
# 	paginator = Paginator(publications, 6)
# 	page = request.GET.get('page')
# 	try:
# 		publications = paginator.page(page)
# 	except PageNotAnInteger:
# 		publications = paginator.page(1)
# 	except EmptyPage:
# 		if request.is_ajax():
# 			return HttpResponse('')
# 		publications = paginator.page(paginator.num_pages)
# 	if request.is_ajax():
# 		return render(request, 'includes/list_ajax.html', { 'publications': publications})
# 	return render(request, 'account/profile.html', {'publications': publications})

