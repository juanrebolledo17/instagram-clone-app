from django import template
from django.utils.safestring import mark_safe
from publications.models import Publication
from account.models import CustomUser
from actions.models import Action

register = template.Library()

@register.filter(name="strip")
def strip_text(text):
	return text.strip()

@register.simple_tag(name="pubs_minus_one")
def number_of_publications_minus_one(pub_id):
	pub = Publication.objects.get(id=pub_id)
	number_of_users_likes = pub.users_like.count()
	number_minus_one = int(number_of_users_likes) - 1
	if (number_minus_one < 0): 
		return 0
	return number_minus_one

@register.simple_tag(takes_context=True)
def get_user_notifications(context, count=5):
	user = context['user']
	actions = Action.objects.exclude(user=user)
	following_ids = user.following.values_list('id', flat=True)
	if following_ids:
		actions = actions.filter(user_id__in=following_ids)
		return actions[:count]
	return None
	
