from django.contrib import admin
from .models import CustomUser, Profile
from django.contrib.auth.admin import UserAdmin

class ProfileAdmin(admin.ModelAdmin):
  list_display = ('user', 'date_of_birth', 'avatar', 'biography', 'phone')

admin.site.register(CustomUser, UserAdmin)
admin.site.register(Profile, ProfileAdmin)
