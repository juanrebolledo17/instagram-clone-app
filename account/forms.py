from django import forms
from django.forms import ModelForm
from .models import CustomUser, Profile
from django.contrib.auth import get_user_model, password_validation

class RegistrationForm(ModelForm):
	password = forms.CharField(label='Password', widget=forms.PasswordInput)
	confirm_password = forms.CharField(label='Confirm password', widget=forms.PasswordInput)

	class Meta:
		model = CustomUser
		fields = ['first_name', 'last_name', 'username', 'email']

	def clean_confirm_password(self):
		cd = self.cleaned_data

		if cd['password'] != cd['confirm_password']:
			raise forms.ValidationError('The fields password and confirm password don\'t match.')

		return cd['password']

class UserEditForm(ModelForm):
	class Meta:
		model = CustomUser
		fields = ('first_name', 'last_name', 'username', 'email')

class ProfileEditForm(ModelForm):
	class Meta:
		model = Profile
		fields = ('date_of_birth', 'avatar', 'biography', 'phone')

class SetPasswordForm(forms.Form):
	error_messages = {
    'password_mismatch': "The two password fields didn't match.",
	}

	new_password1 = forms.CharField(
    label="New password",
    widget=forms.PasswordInput,
    strip=False,
    help_text=password_validation.password_validators_help_text_html(),
	)
	new_password2 = forms.CharField(
    label="New password confirmation",
    strip=False,
    widget=forms.PasswordInput,
	)

	def __init__(self, user, *args, **kwargs):
		self.user = user
		super().__init__(*args, **kwargs)

	def clean_new_password2(self):
		password1 = self.cleaned_data.get('new_password1')
		password2 = self.cleaned_data.get('new_password2')

		if password1 and password2:
			if password1 != password2:
				raise forms.ValidationError(
					self.error_messages['password_mismatch'],
					code='password_mismatch',
				)
			password_validation.validate_password(password2, self.user)
		return password2

	def save(self, commit=True):
		password = self.cleaned_data["new_password1"]
		self.user.set_password(password)
		if commit:
			self.user.save()
		return self.user

class PasswordChangeForm(SetPasswordForm):
	error_messages = {
    **SetPasswordForm.error_messages,
    'password_incorrect': "Your old password was entered incorrectly. Please enter it again.",
	}
	old_password = forms.CharField(
    label="Old password",
    strip=False,
    widget=forms.PasswordInput(attrs={'autofocus': True}),
	)

	field_order = ['old_password', 'new_password1', 'new_password2']

	def clean_old_password(self):
		old_password = self.cleaned_data["old_password"]

		if not self.user.check_password(old_password):
		  raise forms.ValidationError(
		    self.error_messages['password_incorrect'],
		    code='password_incorrect',
		  )
		return old_password
	
