from django.contrib import admin
from .models import Publication, Comment 

class PublicationAdmin(admin.ModelAdmin):
	list_display = ['description', 'user', 'image', 'created']
	list_filter = ['created']

class CommentAdmin(admin.ModelAdmin):
	list_display = ['publication', 'created', 'active']
	list_filter = ['created', 'active', 'updated']
	search_fields = ['body']

admin.site.register(Publication, PublicationAdmin)
admin.site.register(Comment, CommentAdmin)


