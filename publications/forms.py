from django import forms
from .models import Publication, Comment

class CommentForm(forms.ModelForm):
	class Meta:
		model = Comment
		fields = ['body']

class PublicationCreateForm(forms.ModelForm):
	class Meta:
		model = Publication
		fields = ['image', 'description'] # , 'url'
		# widgets = {
		# 	'url': forms.HiddenInput
		# }

	# def clean_url(self):
	# 	url = self.cleaned_data['url']
	# 	valid_extensions = ['jpg', 'jpeg', 'png']
	# 	extension = url.rsplit('.', 1)[1].lower()
	# 	if extension not in valid_extensions:
	# 		raise forms.ValidationError('The given URL does not match valid image extensions.')
	# 	return url

	# def __init__(self, user, *args, **kwargs):
	# 	self.user = user
	# 	super().__init__(*args, **kwargs)