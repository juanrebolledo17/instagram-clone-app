# Generated by Django 2.2.2 on 2019-10-31 23:40

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('publications', '0009_comment_user'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='comment',
            name='name',
        ),
    ]
