from django.db import models
from django.conf import settings

def user_directory_path(instance, filename):
	return 'users/{0}/publications/{1}'.format(instance.user.username, filename)

class Publication(models.Model):
	user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="publications_created", on_delete="CASCADE")
	description = models.TextField(blank=True, null=True)
	image = models.ImageField(upload_to='users/publications/', blank=True)
	created = models.DateField(auto_now_add=True, db_index=True)
	users_like = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name="publications_liked", blank=True)
	# slug = models.SlugField(max_length=200, blank=True)
	# url = models.URLField(blank=True, null=True)
	# this works
	# image = models.ImageField(upload_to='users/publications/', blank=True)

	def __str__(self):
		return self.description

	class Meta:
		get_latest_by = ['created']

class Comment(models.Model):
	user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="publications_comment", on_delete="CASCADE", null=True)
	publication = models.ForeignKey(Publication, related_name='comments', on_delete='CASCADE')
	body = models.TextField()
	created = models.DateTimeField(auto_now_add=True)
	updated = models.DateTimeField(auto_now=True)
	active = models.BooleanField(default=True)

	class Meta: 
		ordering = ['created']

	def __str__(self):
		return 'Comment by {} on {}'.format(self.user, self.publication)

