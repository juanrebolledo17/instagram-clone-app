from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from .forms import PublicationCreateForm, CommentForm
from django.http import JsonResponse, HttpResponse
from django.views.decorators.http import require_POST
import json
from .models import Publication, Comment
from common.decorators import ajax_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from actions.utils import create_action

@login_required
def create_publication(request):
	if request.method == 'POST':
		form = PublicationCreateForm(data=request.POST, files=request.FILES)
		if form.is_valid():
			new_item = form.save(commit=False)
			new_item.user = request.user
			new_item.save()
			create_action(request.user, 'create a publication', new_item)
			return redirect('publications:create_publication')
	else:
		form = PublicationCreateForm()

	return render(request, 'publications/publication/create.html', {'section': 'publications', 'form': form})

@ajax_required
@login_required
@require_POST
def publication_like(request):
	if request.method == 'POST':
		data = json.loads(request.body.decode('utf-8'))
		pub_id = data.get('id')
		action = data.get('action')
		if pub_id and action:
			try:
				pub = Publication.objects.get(id=pub_id)
				if action == 'like':
					pub.users_like.add(request.user)
					create_action(request.user, 'likes', pub)
				else:
					pub.users_like.remove(request.user)
				return JsonResponse({'status': 'ok'})
			except:
				pass
		return JsonResponse({'status': 'ko'})

@login_required
def publication_list(request):
	publications = Publication.objects.all()
	paginator = Paginator(publications, 6)
	page = request.GET.get('page')
	try:
		publications = paginator.page(page)
	except PageNotAnInteger:
		publications = paginator.page(1)
	except EmptyPage:
		if request.is_ajax():
			return HttpResponse('')
		publications = paginator.page(paginator.num_pages)
	if request.is_ajax():
		return render(request, 'publications/publication/list_ajax.html', { 'publications': publications})
	return render(request, 'publications/publication/list.html', {'publications': publications})

# def get_users_likes(request):
# 	if request.method == 'POST':
# 		users_likes = {}
# 		data = json.loads(request.body.decode('utf-8'))
# 		pub_id = data.get('id')
# 		pub = Publication.objects.get(id=pub_id)
# 		users_likes['users_likes'] = pub.users_like.all
# 		return JsonResponse({})

@login_required
def publication_comments_list(request):
	pub_id = request.POST.get('pub_id')
	publication = get_object_or_404(Publication, id=pub_id)

	if request.method == 'POST':
		comment_form = CommentForm(data=request.POST)
		if comment_form.is_valid():
			new_comment = comment_form.save(commit=False)
			new_comment.publication = publication
			new_comment.user = request.user
			new_comment.save()
			return redirect('account:profile')
	# else:
	# 	comment_form = CommentForm()
	# return render(request, 'publications/publication/comment_list.html', {'comments': comments, 'comment_form': comment_form})
	return redirect('account:dashboard')

@login_required
def delete_comment(request):
	if request.method == 'POST':
		comment_id = int(request.POST.get('comment_id'))
		comment = Comment.objects.get(id=comment_id)
		comment.delete()
	return redirect('account:profile')







